const ICON_DATA = require('./data.json')

const ICON_KEYS = Object.keys(ICON_DATA)

const ICON_TOKENS = ICON_KEYS.map(s => ({
  token: ':' + s.toLowerCase() + ':',
  key: s
}))

const ICON_TOKEN_DETECTOR = /:[-_A-Z0-9]+:/i

/**
 * Renvoie les données d'un icones à partir de son identifiant
 * @param sId {string} identifiant de l'icone recherché
 * @returns {*|string}
 */
function getIconData (sId) {
  return (sId in ICON_DATA) ? ICON_DATA[sId] : null
}

/**
 * Renvoie la valeur d'une icone, cette valeur dépend du type
 * type 'unicode' : la fonction renvoi directement le caractère à afficher
 * type 'fontawesome' : la fonction renvoi le code-icon à inclure dans une balise <icon> de font awesome
 * @param sId {string}
 * @param sType
 */
function getIcon (sId, sType) {
  const d = getIconData(sId)
  if (sType in d) {
    return d[sType]
  } else {
    console.warn('this icon type is unknown "%s", available types are [%s]', sType, Object.keys(d).join(', '))
    throw new Error('ERR_ICON_TYPE_UNKNOWN')
  }
}

/**
 * Renvoie true si la chaine contient au moins un token d'icones
 * @param sText {string}
 * @returns {boolean}
 */
function isReplacementWorthy (sText) {
  return Boolean(sText.match(ICON_TOKEN_DETECTOR))
}

/**
 * Remplace les token d'icone (comme :warning: ou :error:) par leur valeur véritable tirée des données (config.json)
 * @param sText {string} texte initial sensé contenir des token d'icones
 * @param sType {string} type d'icone qu'on souhaite rendre (fontawesome, unicode)
 * @returns {string}
 */
function replaceIconTokens (sText, sType = 'unicode') {
  if (isReplacementWorthy(sText)) {
    ICON_TOKENS.forEach(({ token, key }) => {
      sText = sText.replace(token, getIcon(key, sType))
    })
  }
  return sText
}

module.exports = {
  isReplacementWorthy,
  replaceIconTokens
}
