/**
 * Configure le chemin de socket pour synchroniser le serveur et le client
 */

module.exports = {
  SOCKET_PATH: '/socket'
}
