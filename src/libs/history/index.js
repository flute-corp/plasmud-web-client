class History {
  constructor () {
    this._maxLines = 32
    this._history = []
    this._index = 0
    this._last = {
      line: '',
      mod: null
    }
  }

  set bufferLength (value) {
    this._maxLines = value
    while (this._history.length > this._maxLines) {
      this._history.shift()
      --this._index
    }
    this.clampIndex()
  }

  get bufferLength () {
    return this._maxLines
  }

  pushLine (sLine) {
    this._history.push({
      line: sLine,
      mod: null
    })
    while (this._history.length > this._maxLines) {
      this._history.shift()
    }
    this._index = this._history.length
  }

  clampIndex () {
    this._index = Math.max(0, Math.min(this._history.length, this._index))
  }

  get current () {
    if (this._index < this._history.length) {
      return this._history[this._index]
    } else {
      return this._last
    }
  }

  get currentString () {
    const h = this.current
    return h.mod === null
      ? h.line
      : h.mod
  }

  get lastString () {
    if (this._history.length > 0) {
      const h = this._history[this._history.length - 1]
      return h.mod === null
        ? h.line
        : h.mod
    } else {
      return ''
    }
  }

  prev () {
    --this._index
    this.clampIndex()
  }

  next () {
    ++this._index
    this.clampIndex()
  }

  modify (sLine) {
    this.current.mod = sLine
  }

  submit () {
    if (this.currentString !== '' && this.currentString !== this.lastString) {
      this.pushLine(this.currentString)
    }
    this._history.forEach(h => {
      h.mod = null
    })
    this._last.line = ''
    this._last.mod = null
  }

  reset () {
    this._index = this._history.length
  }
}

module.exports = History
