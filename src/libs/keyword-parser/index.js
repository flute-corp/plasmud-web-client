class KeywordParser {
  constructor () {
    this._aParsers = {}
  }

  static buildParser (sSymbolicExpression) {
    const sRegExpNumeric = '[-+]?([0-9]*[.])?[0-9]+([eE][-+]?\\d+)?'
    const sRegExpAny = '.+'
    const sRet = sSymbolicExpression
      .trim()
      .replace(/ +/g, ' ')
      .replace(/\*/g, '(' + sRegExpAny + ')')
      .replace(/#/g, '(' + sRegExpNumeric + ')')
    return new RegExp(sRet, 'i')
  }

  getParser (sSymbolicExpression) {
    const p = this._aParsers
    if (!(sSymbolicExpression in p)) {
      p[sSymbolicExpression] = KeywordParser.buildParser(sSymbolicExpression)
    }
    return p[sSymbolicExpression]
  }

  parse (s, sSymbolicExpression = '*') {
    const rParser = this.getParser(sSymbolicExpression)
    const aMatch = s
      .trim()
      .replace(/ +/g, ' ')
      .match(rParser)
    if (!aMatch) {
      return null
    }
    return aMatch
      .slice(1)
      .filter(x => x !== undefined)
      .map(x => !isNaN(x) ? parseFloat(x) : x)
  }

  dispatch (s, branches) {
    for (const b in branches) {
      if (Object.prototype.hasOwnProperty.call(branches, b)) {
        const pFunc = branches[b]
        const p = this.parse(s, b)
        if (p) {
          return pFunc(...p)
        }
      }
    }
    return null
  }
}

module.exports = KeywordParser
