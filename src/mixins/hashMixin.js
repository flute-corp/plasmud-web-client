import sha256 from 'crypto-js/sha256'

export default {
  methods: {
    hashPassword: function (sPassword) {
      return sha256(sPassword).toString()
    }
  }
}
