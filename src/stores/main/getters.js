export const getters = {
  isFormLoginVisible: state => state.visibility.formLogin,
  isFormPassword: state => state.visibility.formPasswd,
  isConnected: state => state.connected,
  isAuthenticated: state => state.authenticated
}
