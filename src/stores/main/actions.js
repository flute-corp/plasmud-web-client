export const actions = {
  showFormLogin () {
    this.visibility.formLogin = true
  },
  hideFormLogin () {
    this.visibility.formLogin = false
  },
  showFormPassword () {
    this.visibility.formPasswd = true
  },
  hideFormPassword () {
    this.visibility.formPasswd = false
  },
  setConnected (value) {
    this.connected = value
  },
  setAuthenticated (value) {
    this.authenticated = value
  }
}
