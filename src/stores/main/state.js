export const state = () => ({
  visibility: {
    formLogin: false,
    formPasswd: false
  },
  connected: false,
  authenticated: false
})
