export const state = () => ({
  channels: [],
  maxLines: 300,
  selectedChannelHistory: [],
  maxSelectedChannelHistory: 128
})
