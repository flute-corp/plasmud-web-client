/**
 * @typedef ChatStoreChannel {object}
 * @property lines {string[]}
 * @property id {string}
 * @property name {string}
 *
 */
export const getters = {
  getSelectedChannelId: state => {
    const sch = state.selectedChannelHistory
    return sch.length > 0
      ? sch[sch.length - 1]
      : ''
  },

  getChannelList: state => state.channels.map(({ id, name }) => ({ id, name })),

  /**
   * @param state {*}
   * @returns {ChatStoreChannel | undefined}
   */
  getSelectedChannel: state => state.channels.find(c => c.id === state.getSelectedChannelId),

  /**
   *
   * @param state
   * @returns {string[]}
   */
  getSelectedChannelLines: state => state.getSelectedChannel?.lines || []
}
