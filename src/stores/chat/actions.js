function trimArray (a, n) {
  while (a.length >  n) {
    a.shift()
  }
}

let LAST_INDEX = 0

export const actions = {
  createChannel (id, name, go = false) {
    this.channels.push({
      id,
      name,
      lines: []
    })
    if (go) {
      this.selectChannel(id)
    }
  },

  selectChannel (id) {
    this.selectedChannelHistory.push(id)
    trimArray(this.selectedChannelHistory, this.maxSelectedChannelHistory)
  },

  destroyChannel (id) {
    const n = this.channels.findIndex(c => c.id === id)
    if (id === this.getSelectedChannelId) {
      this.selectedChannelHistory.pop()
    }
    if (n >= 0) {
      this.channels.splice(n, 1)
    }
  },

  writeMessage (message, channelId) {
    const oChannel = this.channels.find(c => c.id === channelId)
    if (oChannel) {
      const lines = oChannel.lines
      lines.push({ index: LAST_INDEX++, content: message })
      trimArray(lines, this.maxLines)
    }
  },

  /**
   * Ne modifie pas le state : cette action est destinée à être écoutée par le plugin de store
   * @param message {string}
   * @param channelId {string}
   */
  submitChatMessage (message, channelId) {
  }
}
