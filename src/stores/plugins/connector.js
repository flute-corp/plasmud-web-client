import WSConnector from '../../libs/ws-connector'
import quoteSplit from '../../libs/quote-split'
import { SOCKET_PATH } from 'src/libs/socket-path'
import Ansi256 from '../../libs/ansi-256-colors'

class CommandRepository {

  constructor () {
    this._connector = new WSConnector()
    this._stores = {}
    this._username = ''
    this._ansi256 = new Ansi256()

    const sSocketPath = this._connector.path = SOCKET_PATH || null
    if (sSocketPath) {
      console.info('socket path is', sSocketPath)
      this._connector.path = sSocketPath
    }

    this._connector.events.on('disconnect', () => {
      console.warn('Connection with server has been closed.')
      this.termPrint('Connection with server has been closed.')
      this._stores.main.setConnected(false)
    })

    this._connector.events.on('message', ({ content }) => {
      // déterminer pour qui ce message est destiné
      const aWords = content.split(' ')
      if (aWords.length > 0) {
        switch (aWords[0].toUpperCase()) {
          case 'CHAT:JOIN': {
            const channel = aWords[1]
            this._stores.chat.createChannel(channel, channel, true)
            break
          }
          case 'CHAT:LEAVE': {
            const channel = aWords[1]
            this._stores.chat.destroyChannel(channel)
            break
          }
          case 'CHAT:MSG': {
            const channel = aWords[1]
            const content = aWords.slice(2).join(' ')
            this._stores.chat.writeMessage(content, channel)
            break
          }
          default: {
            return this.termPrint(content)
          }
        }
      }
    })
  }

  get stores () {
    return this._stores
  }

  termPrint (s) {
    this._stores.terminal.printLine(this._ansi256.render(s))
  }


  async _send (sCommand, ...aArgs) {
    if (this._connector.connected) {
      return this._connector.send(sCommand + ' ' + aArgs.join(' '))
    }
  }

  async _connect () {
    try {
      if (this._connector.connected) {
        return
      }
      if (!this._connector.connecting) {
        await this.termPrint('Connecting...')
        await this._connector.connect()
        this._stores.main.setConnected(true)
        await this.termPrint('[#2F2]Connected to ' + this._connector.remoteAddress)
      } else {
        this.termPrint('Still attempting to connect...')
      }
    } catch (e) {
      await this.termPrint('[#D00]' + e.message)
    }
  }

  /**
   * Indentification de l'utilisateur.
   * @param sIdentifier {string} nom de compte de l'utilisateur
   * @param sPassword {string} Mot de passe
   */
  async cmdLogin (sIdentifier = '', sPassword = '') {
    if (sIdentifier === '') {
      this._stores.main.showFormLogin()
      return
    }
    this._username = sIdentifier
    await this._connect()
    return this._send('login', this._username, sPassword)
  }

  async cmdPasswd (sPassword) {
    try {
      if (this._connector.connected) {
        if (sPassword) {
          return this._send('passwd', sPassword)
        } else {
          this._stores.main.showFormPassword()
        }
      }
    } catch (e) {
      await this.termPrint('[#D00]' + e.message)
    }
  }

  parseCommandLine (sInput, ns) {
    const sLine = sInput.trim()
    const aParams = quoteSplit(sLine) // sLine.split(' ');
    // extract command
    const sCommand = aParams.shift().toLowerCase()
    const sMeth = ns + sCommand.charAt(0).toUpperCase() + sCommand.substring(1).toLowerCase()
    return {
      line: sLine,
      method: sMeth,
      command: sCommand,
      params: aParams
    }
  }

  async command (sInput, bEcho) {
    try {
      if (bEcho) {
        await this.termPrint('[#FF4] > ' + sInput)
      }
      const { params, command, method } = this.parseCommandLine(sInput, 'cmd')
      // send command to server
      if (method in this) {
        await this[method](...params)
      } else {
        await this._send(command, ...params)
      }
    } catch (e) {
      await this.termPrint('[#D00]' + e.message)
      console.error(sInput, 'has been rejected :', e.message)
      console.error(e)
    }
  }

  cmdChatJoin (sCurrentChannel, sChannel) {
    return this._send('chat-join', sChannel)
  }

  cmdChatLeave (sCurrentChannel) {
    if (sCurrentChannel === 'lobby') {
      this._stores.chat.writeMessage(
        this._ansi256.render('[#FF4] lobby is the main channel and cannot be quit.'),
        this._stores.chat.getSelectedChannelId
      )
    } else {
      return this._send('chat-leave', sCurrentChannel)
    }
  }

  cmdChatMsg (sCurrentChannel, sDestUserId, ...message) {
    return this._send('chat-msg', sDestUserId, ...message)
  }

  async chatCommand (sInput, sChannel) {
    try {
      sInput = sInput.trim()
      const bInitialSlash = sInput.startsWith('/')
      if (bInitialSlash) {
        // chat command like /join /msg ...
        sInput = sInput.substring(1) // remove /
      } else {
        // normal txat message : must prepend "say" !
        sInput = 'say ' + sInput
      }
      const { params, command, method } = this.parseCommandLine(sInput, 'cmdChat')
      // send command to server
      params.unshift(sChannel)
      if (method in this) {
        await this[method](...params)
      } else {
        await this._send('chat/' + command, ...params)
      }
    } catch (e) {
      console.error(sInput, 'has been rejected :', e.message)
      console.error(e)
    }
  }

  async autoexec (aCommands) {
    const pause = t => new Promise(resolve => {
      setTimeout(resolve, t)
    })
    await pause(300)
    for (const c of aCommands) {
      await this.command(c)
      await pause(200)
    }
  }
}

const cmd = new CommandRepository()

export default ({ store }) => {
  cmd.stores[store.$id] = store

  store.$onAction(async ctx => {
    const { store, name, args } = ctx
    switch (store.$id + '/' + name) {
      case 'terminal/submitCommand': {
        const [ command, echo = false ] = args
        await cmd.command(command, echo)
        if (command.split(' ').shift() === 'login') {
          // await cmd.autoexec([
          //   'enter',
          //   'look'
          // ])
        }
        break
      }

      case 'chat/submitChatMessage': {
        const [ message, channel ] = args
        await cmd.chatCommand(message, channel)
        break
      }

      default: {
        break
      }
    }
  })
}
