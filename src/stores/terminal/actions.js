export const actions = {
  printLine (content) {
    this.lines.push({ content, index: this.index++ })
    while (this.lines.length > this.maxLines) {
      this.lines.shift()
    }
  },

  submitCommand (command, echo = false) {
  },

  setColor ({r, g, b}) {
    const bodyStyles = document.body.style;
    const sTermColorPrefix = '--terminal-color-'
    bodyStyles.setProperty(sTermColorPrefix + 'red', r)
    bodyStyles.setProperty(sTermColorPrefix + 'green', g)
    bodyStyles.setProperty(sTermColorPrefix + 'blue', b)
  }
}
